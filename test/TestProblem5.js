const data = require("../movieData.cjs");
const problem = require("../Problem5");
try {
  const groupedMovies = problem.groupMoviesByGenre(data.favouritesMovies);
  console.log(groupedMovies);
} catch (error) {
  console.log(error);
}
