//Q1. Find all the movies with total earnings more than $500M.
const dataSet = require("./movieData.cjs");

function totalEarningsMoreThan500(data) {
  return Object.keys(data).filter(
    (movie) => Number(data[movie].totalEarnings.slice(1, -1)) > 500
  );
}
module.exports = { totalEarningsMoreThan500 };
