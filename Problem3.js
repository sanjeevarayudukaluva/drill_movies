//Q.3 Find all movies of the actor "Leonardo Dicaprio".
const dataSet = require("./movieData.cjs");

function allMoviesOfLeonardoDicaprio(data) {
  return Object.keys(data).filter((movie) =>
    data[movie].actors.includes("Leonardo Dicaprio")
  );
}
module.exports = { allMoviesOfLeonardoDicaprio };
