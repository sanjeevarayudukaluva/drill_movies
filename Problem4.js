/*
 Q.4 Sort movies (based on IMDB rating)
          if IMDB ratings are same, compare totalEarning as the secondary metric.
*/
const favouritesMovies = require("./movieData.cjs");

function sortMoviesByRatingAndEarnings(data) {
  return Object.keys(data).sort((movieA, movieB) => {
    if (data[movieB].imdbRating !== data[movieA].imdbRating) {
      return data[movieB].imdbRating - data[movieA].imdbRating;
    } else {
      return (
        parseInt(data[movieB].totalEarnings.slice(1, -1)) -
        parseInt(data[movieA].totalEarnings.slice(1, -1))
      );
    }
  });
}
module.exports = { sortMoviesByRatingAndEarnings };
