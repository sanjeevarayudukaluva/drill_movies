const moviesData = require("./movieData.cjs");

function groupMoviesByGenre(movies) {
  const priorityGenre = {};
  const priorityOrder = ["drama", "sci-fi", "adventure", " thriller", "crime"];
  for (let movie in movies) {
    for (let genre of movies[movie].genre) {
      if (priorityOrder.includes(genre)) {
        if (!priorityGenre[genre]) {
          priorityGenre[genre] = [];
        }
        priorityGenre[genre].push(movie);
      }
    }
  }
  return priorityGenre;
}

module.exports = { groupMoviesByGenre };
