// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
const dataSet = require("./movieData.cjs");

function oscarsMoreThanThreeAndEarningsMoreThan500(data) {
  return Object.keys(data).filter(
    (movie) =>
      data[movie].oscarNominations > 3 &&
      Number(data[movie].totalEarnings.slice(1, -1)) > 500
  );
}
module.exports = { oscarsMoreThanThreeAndEarningsMoreThan500 };
